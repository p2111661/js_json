const textChapitre = document.getElementById('chapterText');
const liensChapitre = document.getElementById('chapterLinks');

var numeroCahpitre=1;
var url = './json/chapitre'+numeroCahpitre+'.json'
startFetch(url);

onhashchange = () => {
    numeroCahpitre = location.hash.substring(1);
    url = './json/chapitre'+numeroCahpitre+'.json';
    startFetch(url)
}

//alternative
/*window.addEventListener('hashchange', () => {
    console.log('The hash has changed!')
    numcahp = location.hash.substring(1)
    url = './json/chapitre'+numcahp+'.json';
    tt()
    console.log(url)
});
*/

function startFetch(url){
    fetch(url)
        .then(function(response){
            if(response.ok){
                return response.json(); // une promesse
            }
            else{
                console.log("erreur "+response.status);
            }
        })
        .then(function(data){
            afficheChap(data)
        })
        .catch(function (err) {
            console.log(err);
            
        });
}

function afficheChap(data){
    liensChapitre.innerHTML = "";

    textChapitre.innerHTML = data["txt"];
    data["links"].forEach(element => {
        let temp = document.createElement('a');
        temp.style.display = "block";
        temp.href = element.link ;
        temp.innerHTML = element.txt;
        liensChapitre.appendChild(temp);   
    });
}